function HTMLActuator() {
  this.tileContainer    = document.querySelector(".tile-container");
  this.scoreContainer   = document.querySelector(".score-container");
  this.bestContainer    = document.querySelector(".best-container");
  this.messageContainer = document.querySelector(".game-message");
  this.sharingContainer = document.querySelector(".score-sharing");

  this.score = 0;
}
var shareBestScore = 0;
HTMLActuator.prototype.actuate = function (grid, metadata) {
  var self = this;

  window.requestAnimationFrame(function () {
    self.clearContainer(self.tileContainer);

    grid.cells.forEach(function (column) {
      column.forEach(function (cell) {
        if (cell) {
          self.addTile(cell);
        }
      });
    });

    self.updateScore(metadata.score);
    self.updateBestScore(metadata.bestScore);

    if (metadata.terminated) {
      if (metadata.over) {
        self.message(false); // You lose
      } else if (metadata.won) {
        self.message(true); // You win!
      }
    }

  });
};

// Continues the game (both restart and keep playing)
HTMLActuator.prototype.continueGame = function () {
  if (typeof ga !== "undefined") {
    ga("send", "event", "game", "restart");
  }

  this.clearMessage();
};

HTMLActuator.prototype.clearContainer = function (container) {
  while (container.firstChild) {
    container.removeChild(container.firstChild);
  }
};

HTMLActuator.prototype.addTile = function (tile) {
  var self = this;

  var wrapper   = document.createElement("div");
  var inner     = document.createElement("div");
  var position  = tile.previousPosition || { x: tile.x, y: tile.y };
  var positionClass = this.positionClass(position);

  // We can't use classlist because it somehow glitches when replacing classes
  var classes = ["tile", "tile-" + tile.value, positionClass];

  if (tile.value > 2048) classes.push("tile-super");

  this.applyClasses(wrapper, classes);

  inner.classList.add("tile-inner");
  inner.textContent = tile.value;

  if (tile.previousPosition) {
    // Make sure that the tile gets rendered in the previous position first
    window.requestAnimationFrame(function () {
      classes[2] = self.positionClass({ x: tile.x, y: tile.y });
      self.applyClasses(wrapper, classes); // Update the position
    });
  } else if (tile.mergedFrom) {
    classes.push("tile-merged");
    this.applyClasses(wrapper, classes);

    // Render the tiles that merged
    tile.mergedFrom.forEach(function (merged) {
      self.addTile(merged);
    });
  } else {
    classes.push("tile-new");
    this.applyClasses(wrapper, classes);
  }

  // Add the inner part of the tile to the wrapper
  wrapper.appendChild(inner);

  // Put the tile on the board
  this.tileContainer.appendChild(wrapper);
};

HTMLActuator.prototype.applyClasses = function (element, classes) {
  element.setAttribute("class", classes.join(" "));
};

HTMLActuator.prototype.normalizePosition = function (position) {
  return { x: position.x + 1, y: position.y + 1 };
};

HTMLActuator.prototype.positionClass = function (position) {
  position = this.normalizePosition(position);
  return "tile-position-" + position.x + "-" + position.y;
};

HTMLActuator.prototype.updateScore = function (score) {
  this.clearContainer(this.scoreContainer);

  var difference = score - this.score;
  this.score = score;

  this.scoreContainer.textContent = this.score;

  if (difference > 0) {
    var addition = document.createElement("div");
    addition.classList.add("score-addition");
    addition.textContent = "+" + difference;

    this.scoreContainer.appendChild(addition);
  }
};

HTMLActuator.prototype.updateBestScore = function (bestScore) {
  shareBestScore  = bestScore;
  this.bestContainer.textContent = bestScore;
};

HTMLActuator.prototype.message = function (won) {
  var type    = won ? "game-won" : "game-over";
  var message = won ? "You win!" : "Game over!";

  if (typeof ga !== "undefined") {
    ga("send", "event", "game", "end", type, this.score);
  }

  this.messageContainer.classList.add(type);
  this.messageContainer.getElementsByTagName("p")[0].textContent = message;

  this.clearContainer(this.sharingContainer);
  this.sharingContainer.appendChild(this.scoreFacebookButton());
  this.sharingContainer.appendChild(this.scoreTweetButton());
  this.sharingContainer.appendChild(this.submitScore());
  twttr.widgets.load();
};

HTMLActuator.prototype.clearMessage = function () {
  // IE only takes one value to remove at a time.
  this.messageContainer.classList.remove("game-won");
  this.messageContainer.classList.remove("game-over");
};

HTMLActuator.prototype.scoreTweetButton = function () {
  var tweet = document.createElement("a");  
  var text = "I scored " + shareBestScore   + " points at 2048, a game where you " +
             "join numbers to score high! #2048game";
  tweet.classList.add("twitter");    
  tweet.setAttribute("data-via", "SmartyGames");
  tweet.textContent = "Twitt it";
  tweet.onclick= function(){
	 var width  = 575,
        height = 400,
        left   = ($(window).width()  - width)  / 2,
        top    = ($(window).height() - height) / 2,
        url    = "https://twitter.com/share?text="+text,
        opts   = 'status=1' +
                 ',width='  + width  +
                 ',height=' + height +
                 ',top='    + top    +
                 ',left='   + left;
    
    window.open(url, 'twitter', opts);
 
    return false;
  }
  tweet.setAttribute("data-text", text);
  return tweet;
};
HTMLActuator.prototype.submitScore = function(){
	var submitScore = document.createElement("a");
	submitScore.classList.add("facebook-share-button");
	submitScore.textContent = "Submit score";
	submitScore.setAttribute("href", "javascript:void(0);");
	submitScore.onclick = function(){
		 var $dialog = $('#submitScoreContainer')
		  .load('submitScore.html')
		  .dialog({
			  autoOpen: false,			  
			  title: 'Submit score',
			  width: 500,
			  minHeight: 300,
			  maxHeight: 550
		  });
		$dialog.dialog('open');
	}
	return submitScore;
}

HTMLActuator.prototype.scoreFacebookButton = function () {
  var fbPost = document.createElement("a");
  fbPost.classList.add("facebook-share-button");
  fbPost.setAttribute("href", "javascript:void(0);");    
  fbPost.textContent = "Facebook"; 
  var myScore = shareBestScore;
  if(typeof(myScore) == 'undefined')
	myScore = 0;
  fbPost.onclick = function () {		
		var textMessage = "I scored " + myScore + " points at 2048, a game where you " + "join numbers to score high! #2048tile";			 
		FB.ui({
		   method: 'feed',
		   link: 'http://www.2048tile.co',
		   name: '2048 Tile - Brain Training Game',
		   picture: 'http://www.2048tile.co/og_image.png',
		   caption: 'I took up the challenge at 2048 tile!',
		   description: textMessage
		 }, function(response){});
  }
  return fbPost;
};