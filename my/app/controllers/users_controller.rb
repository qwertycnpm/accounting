# encoding: utf-8
class UsersController < ApplicationController

before_action :authorize_admin!, only: [:index, :destroy ]
before_action :set_user, only: [:show, :edit, :destroy, :update]
before_action :user_authentication, only: [:show, :edit ]


  def new
    @user = User.new
  end
  
  def update
    if @user.update(user_params)
      flash[:notice] = "Thông tin tài khoản đã được cập nhật."
      redirect_to @user
    else
      flash[:alert] = "Cập nhật thất bại."
      render "edit"
    end
  end
  
  
  
  
  def destroy
    if current_user == @user 
      flash[:notice] = "Không thể xóa người dùng hiện tại."
    else 
      @user.destroy
    end
     redirect_to (:back)
  end
  
  
  

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:notice] = "Đăng kí thành công"
      redirect_to users_path
    else
      render :new
    end
  end
  
  def index
    @users = User.all
  end
  
  def show
  end
  
  def edit
  end

  private
  def user_authentication
    if @user != current_user and !current_user.isAdmin!
      redirect_to user_path(current_user)
    end
  end
  
  def set_user
    @user = User.find(params[:id])
  end
  
  def user_params
    params.require(:user).permit(:name,:password, :email, :password_confirmation, :per)
  end
end
