# encoding: utf-8
class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.where(:name => params[:signin][:name]).first
    if user && user.authenticate(params[:signin][:password])
      session[:user_id] = user.id
      flash[:notice] = "Đăng nhập thành công!"
      redirect_to root_url
    else
      flash[:error] = "Tài khoản hoặc mật khảu chưa đúng."
      render :new
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:notice] = "Bạn vừa đăng xuất."
    redirect_to root_url
  end
end
