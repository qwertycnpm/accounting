class Admin::BaseController < ApplicationController
  #before_action :authorize_admin!
  def index
  end

  def create
    params = user_params.dup
    params[:password_confirmation] = params[:password]
    @user = User.new(params)
    if @user.save
      flash[:notice] = "User has been created."
      redirect_to admin_users_path
    else
      flash.now[:alert] = "User has not been created."
      render :action => "new"
    end
  end
  private

  def user_params
    params.require(:user).permit(:name,
    :password,
    :password_confirmation)
  end
end