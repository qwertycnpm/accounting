# encoding: utf-8
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  private
  def require_signin!
    if current_user.nil?
      flash[:error] =
      "Bạn cần đăng nhập trước."
      redirect_to signin_path
    end
  end
  helper_method :require_signin!

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  helper_method :current_user

  private

  def authorize_admin!
    require_signin!
    unless (current_user.per == "admin")
      flash[:alert] = "Chỉ cho phép quản trị viên."
      redirect_to user_path(current_user)
    end
  end

  def authorize_account!
    require_signin!
    unless (current_user.per == "account")
      flash[:alert] = "Chỉ cho phép kế toán."
      redirect_to (:back)
    end
  end

 

end