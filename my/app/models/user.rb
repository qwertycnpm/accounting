class User < ActiveRecord::Base
  has_secure_password
  validates :name ,presence: true, uniqueness: true
  validates :email, presence: true
  
  def to_s
    "#{name} (#{per})"
  end
  
  def isAdmin!
    return per == "admin"
  end
  
  def isAccount!
    return per == "account"
  end
  
  
  
end
