PhanMemKeToan::Application.routes.draw do

  get "users/index"
  root "main#index"
  resources :phieuchis do
    get "/thanhtoan", to: "phieuthus#thanhtoan"
  end
  
  resources :phieuthus
  resources :users
  delete "/signout", to: "sessions#destroy", as: "signout"
  get "/signin", to: "sessions#new"
  post "/signin", to: "sessions#create"
  post "/signout", to: "sessions#destroy"
  namespace :admin do
    root :to => "base#index"
    resources :users
  end
end
