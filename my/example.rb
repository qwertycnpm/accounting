def in_words(integer)
    ones_array = [' zero ','one','two','three','four','five','six','seven','eight','nine']
    teens_array = ['blank','eleven','twelve','thirteen','fourteen','fifteen','sixteen','seventeen','eighteen','nineteen']
    tens_array = ['blank','ten','twenty','thirty','fourty','fifty','sixty','seventy','eighty','ninety']

    result = ""

    #FOR MILLIONS
    writing = integer / 1_000_000_000
    left_over = integer % 1_000_000_000

    if writing > 0
      result << in_words(writing) << " billion "
    integer = left_over
    end

    #FOR MILLIONS
    writing = integer / 1_000_000
    left_over = integer % 1_000_000

    if writing > 0
      result << in_words(writing) << " million "
    integer = left_over
    end

    #FOR THOUSANDS
    writing = integer / 1000
    left_over = integer % 1000

    if writing > 0
      result << in_words(writing) << " thousand "
    integer = left_over
    end

    #FOR HUNDREDS
    writing = integer / 100
    left_over = integer % 100

    if writing > 0
      result << in_words(writing) << " hundred "
    integer = left_over
    end

    #FOR TENS!!!
    writing = integer / 10
    left_over = integer % 10

    if writing > 0
      if writing == 1 && left_over > 0
      result << teens_array[left_over]
      left_over = 0
      else
      result << tens_array[writing]
      end
    end

    writing = left_over

    #FOR ONES!!!!
    if writing > 0
    result << ones_array[writing]
    end

    result

  end
  
puts in_words(1000)
puts in_words(10500)
puts in_words(10012520)
