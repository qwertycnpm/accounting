﻿module AuthenticationHelpers

	def sign_in_as!(user)
		visit '/signin'
		fill_in "Tên đăng nhập", with: user.name
		fill_in "Mật khẩu", with: user.password
		click_button 'Đăng nhập'
		expect(page).to have_content("Signed in successfully.")
	end
end
RSpec.configure do |c|
	c.include AuthenticationHelpers, type: :feature
end


module AuthHelpers
	def sign_in(user)
		session[:user_id] = user.id
	end
end

RSpec.configure do |c|
	c.include AuthHelpers, type: :controller
end
