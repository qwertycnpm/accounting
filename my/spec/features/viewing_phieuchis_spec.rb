﻿#encoding UTF-8
require 'spec_helper'


feature "Xem Phiếu Chi" do
  scenario "Danh sách phiếu chi" do
    phieuchi = FactoryGirl.create(:phieuchi, so_phieu: "01")
    visit '/'
    click_link '01'
    expect(page.current_url).to eql(phieuchi_url(phieuchi))
  end
end
