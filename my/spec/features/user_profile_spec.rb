﻿require 'spec_helper'

feature "Thông tin người dùng" do
	scenario "Xem thông tin" do
		user = FactoryGirl.create(:user)
		visit user_path(user)
		expect(page).to have_content(user.name)
		expect(page).to have_content(user.email)
	end
end

feature "Chỉnh sửa thông tin" do
	scenario "Cập nhật người dùng" do
		user = FactoryGirl.create(:user)
		visit user_path(user)
		click_link "Sửa thông tin"
		fill_in "Tên người dùng", with: "qwerty"
		click_button "Cập nhật thông tin"
		expect(page).to have_content("Cập nhật thành công")
	end
end