﻿#encoding: UTF-8
require 'spec_helper'

feature 'Tạo phiếu thu' do
	before do 
		sign_in_as!(FactoryGirl.create(:admin_user))
		visit '/'
		click_link 'Tạo phiếu thu'
	end
	
	scenario " phieu thu" do
  
		fill_in 'Số Phiếu' , with: "Số 01"
		fill_in 'Họ Và Tên Người Nhận', with: 'Nguyễn Văn A'
		fill_in 'Địa chỉ', with: 'Khoa Công nghệ Thông Tin, Trường Đại học Công Nghệ'
		fill_in 'Lý do thu', with: 'Kinh phí'
		fill_in 'Số tiền', with: '000000 đ'
		#fill_in 'Viết bằng chữ'
		
	
		click_button 'Tạo phiếu thu'
		expect(page).to have_content('Phiếu thu của bạn đã được tạo')
	end
  
  scenario "Lỗi ! Không thể tạo phiếu thu" do
		click_button 'Tạo phiếu thu'
		
		expect(page).to have_content("Phiếu thu không thể tạo.")
		expect(page).to have_content("So phieu can't be blank")
		expect(page).to have_content("Ho va ten nguoi nhan can't be blank")
		expect(page).to have_content("Ly do thu can't be blank")
		
		expect(page).to have_content ("Dia thu can't be blank")
	end
end