﻿require 'spec_helper'
	feature 'Đăng ký' do
		scenario 'Thành công' do
			visit '/'
			click_link 'Đăng ký'
			fill_in 'Email', with: 'user@example.com'
			fill_in 'Mật khẩu', with: 'password'
			fill_in 'Nhập lại mật khẩu', with: 'password'
			click_button 'Đăng ký'
			expect(page).to have_content("Bạn đã đăng ký thành công")
		end
	end