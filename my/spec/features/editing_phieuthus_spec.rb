﻿require 'spec_helper'

feature "Sửa Phiếu thu" do
	before do
		sign_in_as!(FactoryGirl.create(:admin_user))
		FactoryGirl.create(:phieuthu, so_phieu: "01")

		visit "/"
		click_link "01"
		click_link "Sửa Phiếu thu"
	end
	
	scenario "Cập nhật phiếu thu" do
		fill_in "Số Phiếu", with: "01"
		click_button "Cập Nhật Phiếu thu"

		expect(page).to have_content("Phiếu thu của bạn đã được cập nhật.")
	end
	
	scenario "Cập nhật Phiếu thu khi lỗi nhập dữ liệu" do
		fill_in "Số Phiếu", with: ""
		click_button "Cập Nhật Phiếu thu"
		expect(page).to have_content("Cập nhật Phiếu thu thất bại.")
	end

end