﻿require 'spec_helper'

feature "Xóa Phiếu Chi" do
	before do
		sign_in_as!(FactoryGirl.create(:admin_user))
	end
  scenario "Xóa 1 Phiếu chi" do
    FactoryGirl.create(:phieuchi, so_phieu: "01")

    visit "/"
    click_link "01"
    click_link "Xóa Phiếu Chi"

    expect(page).to have_content("Phiếu chi của bạn đã được xóa thành công")

    visit "/"

    expect(page).to have_no_content("01")
  end
end
