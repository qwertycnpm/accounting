﻿#encoding UTF-8
require 'spec_helper'


feature "Xem Phiếu thu" do
  scenario "Danh sách phiếu thu" do
    phieuthu = FactoryGirl.create(:phieuthu, so_phieu: "01")
    visit '/'
    click_link '01'
    expect(page.current_url).to eql(phieuthu_url(phieuthu))
  end
end
