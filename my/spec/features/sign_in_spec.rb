﻿require 'spec_helper'
feature "Đăng nhập" do
	scenario 'Đăng nhập' do
		user = FactoryGirl.create(:user, name: 'Tên đăng nhập', email: 'ex@exemple.com')
		visit '/'
		click_link 'Đăng nhập'
		fill_in 'Tên đăng nhập', with: user.name
		fill_in 'Mật khẩu', with: user.password
		click_button "Đăng nhập"
		expect(page).to have_content("Đăng nhập thành công")
	end
end