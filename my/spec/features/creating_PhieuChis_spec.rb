﻿#encoding: UTF-8
require 'spec_helper'

feature 'Tạo phiếu chi' do
	before do 
		sign_in_as!(FactoryGirl.create(:admin_user))
		visit '/'
		click_link 'Tạo phiếu chi'
	end
	
	scenario " phieu chi" do
  
		fill_in 'Số Phiếu' , with: "Số 01"
		fill_in 'Họ Và Tên Người Nhận', with: 'Nguyễn Văn A'
		fill_in 'Địa chỉ', with: 'Khoa Công nghệ Thông Tin, Trường Đại học Công Nghệ'
		fill_in 'Lý do chi', with: 'Kinh phí'
		fill_in 'Số tiền', with: '000000 đ'
		#fill_in 'Viết bằng chữ'
		
	
		click_button 'Tạo phiếu chi'
		expect(page).to have_content('Phiếu chi của bạn đã được tạo')
	end
  
  scenario "Lỗi ! Không thể tạo phiếu chi" do
		click_button 'Tạo phiếu chi'
		
		expect(page).to have_content("Phiếu chi không thể tạo.")
		expect(page).to have_content("So phieu can't be blank")
		expect(page).to have_content("Ho va ten nguoi nhan can't be blank")
		expect(page).to have_content("Ly do chi can't be blank")
		
		expect(page).to have_content ("Dia chi can't be blank")
	end
end