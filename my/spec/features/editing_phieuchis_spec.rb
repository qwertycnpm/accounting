﻿require 'spec_helper'

feature "Sửa Phiếu chi" do
	before do
		sign_in_as!(FactoryGirl.create(:admin_user))
		FactoryGirl.create(:phieuchi, so_phieu: "01")

		visit "/"
		click_link "01"
		click_link "Sửa Phiếu Chi"
	end
	
	scenario "Cập nhật phiếu chi" do
		fill_in "Số Phiếu", with: "01"
		click_button "Cập Nhật Phiếu Chi"

		expect(page).to have_content("Phiếu chi của bạn đã được cập nhật.")
	end
	
	scenario "Cập nhật Phiếu chi khi lỗi nhập dữ liệu" do
		fill_in "Số Phiếu", with: ""
		click_button "Cập Nhật Phiếu Chi"
		expect(page).to have_content("Cập nhật Phiếu chi thất bại.")
	end

end