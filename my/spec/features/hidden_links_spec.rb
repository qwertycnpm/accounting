﻿require 'spec_helper'
feature "hidden links" do
	let(:user) { FactoryGirl.create(:user) }
	let(:admin) { FactoryGirl.create(:admin_user) }
	let(:phieuchi) { FactoryGirl.create(:phieuchi) }
	let(:phieuthu) { FactoryGirl.create(:phieuthu) }
	
	context "anonymous users" do
		scenario "không hiện link tạo mới" do
			visit '/'
			assert_no_link_for "Tạo phiếu chi mới"
			assert_no_link_for "Tạo phiếu thu mới"
		end
		
		scenario "không hiện link sửa" do
			visit phieuchi_path(phieuchi)
			visit phieuthu_path(phieuthu)
			assert_no_link_for "Sửa phiếu thu"
			assert_no_link_for "Sửa phiếu chi"
		end
		
		scenario "không hiện link xóa" do
			visit phieuchi_path(phieuchi)
			visit phieuthu_path(phieuthu)
			assert_no_link_for "Xóa phiếu thu"
			assert_no_link_for "Sửa phiếu chi"
		end
	end
	
	context "Đã đăng ký" do
		before { sign_in_as!(user) }
		scenario "không hiện link tạo mới" do
			visit '/'
			assert_no_link_for "Tạo phiếu chi mới"
			assert_no_link_for "Tạo phiếu thu mới"
		end
		
		scenario "không hiện link sửa" do
			visit phieuchi_path(phieuchi)
			visit phieuthu_path(phieuthu)
			assert_no_link_for "Sửa phiếu thu"
			assert_no_link_for "Sửa phiếu chi"
		end
		
		scenario "không hiện link xóa" do
			visit phieuchi_path(phieuchi)
			visit phieuthu_path(phieuthu)
			assert_no_link_for "Xóa phiếu thu"
			assert_no_link_for "Sửa phiếu chi"
		end
	end
	
	context "admin users" do
		before { sign_in_as!(admin) }
		scenario "Thấy link tạo mới" do
			visit '/'
			assert_link_for "Tạo phiếu chi mới"
			assert_link_for "Tạo phiếu thu mới"
		end
		
		scenario "Thấy link sửa" do
			visit phieuchi_path(phieuchi)
			visit phieuthu_path(phieuthu)
			assert_link_for "Sửa phiếu thu"
			assert_link_for "Sửa phiếu chi"
		end
		
		scenario "Thấy link xóa" do
			visit phieuchi_path(phieuchi)
			visit phieuthu_path(phieuthu)
			assert_link_for "Xóa phiếu thu"
			assert_link_for "Sửa phiếu chi"
		end
	end
end