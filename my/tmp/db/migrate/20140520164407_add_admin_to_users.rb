class AddAdminToUsers < ActiveRecord::Migration
  def change
    add_column :users, :position, :string, default: "nothing"
  end
end
