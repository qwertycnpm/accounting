class User < ActiveRecord::Base
  has_secure_password
  validates :email, presence: true
  def to_s
    "#{email} (#{position})"
  end
  
  def isAdmin!
     return position == "admin"
  end
end
