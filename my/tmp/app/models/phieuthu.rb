class Phieuthu < ActiveRecord::Base
	validates :so_phieu ,presence: true, uniqueness: true
	validates :ho_va_ten_nguoi_nop , presence: true
	validates :dia_chi, presence: true
	validates :ly_do_thu , presence: true
	validates :so_tien , numericality: { greater_than: 0 }
end
