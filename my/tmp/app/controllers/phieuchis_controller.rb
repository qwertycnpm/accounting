﻿class PhieuchisController < ApplicationController
  #before_action :authorize_admin!, except: [:index, :show]
  before_action :set_phieuchi, only: [:show, :edit, :update, :destroy ]
  def index
    @phieuchis = Phieuchi.all
  end

  def new
    @phieuchi = Phieuchi.new
  end

  def create
    @phieuchi = Phieuchi.new(phieuchi_params)
    if @phieuchi.save
      flash[:notice] = "Phiếu chi của bạn đã được tạo"
      redirect_to @phieuchi
    else
      flash[:alert] = "Phiếu chi không thể tạo."
      render "new"
    end
  end

  def show
    @phieuchi = Phieuchi.find(params[:id])
  end

  def edit
    @phieuchi = Phieuchi.find(params[:id])
  end

  def thanhtoan
    @phieuchi = Phieuchi.find(params[:phieuchi_id])
  rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Phiếu chi bạn tìm không thấy"
    
    
    @phieuthu = Phieuthu.new(phieuthu_params)
    if @phieuthu.save
      flash[:notice] = "Phiếu thu của bạn đã được tạo"
      redirect_to @phieuthu
    else
      flash[:alert] = "Phiếu thu không thể tạo."

      render "thanhtoan"
    end
    redirect_to phieuchis_path
    end

  def update
    @phieuchi = Phieuchi.find(params[:id])
    if @phieuchi.update(phieuchi_params)
      flash[:notice] = "Phiếu chi của ban đã được cập nhật."
      redirect_to @phieuchi
    else
      flash[:alert] = "Cập nhật Phiếu chi thất bại."
      render "edit"
    end
  end

  def destroy
    @phieuchi = Phieuchi.find(params[:id])
    @phieuchi.destroy
    flash[:notice] = "Phiếu chi của bạn đã được xóa thành công"
    redirect_to phieuchis_path
  end

  private

  def phieuchi_params
    params.require(:phieuchi).permit(:so_phieu ,:ho_va_ten_nguoi_nhan, :dia_chi, :ly_do_chi , :so_tien )
  end

  private

  def phieuthu_params
    params.require(:phieuthu).permit(:so_phieu, :ho_va_ten_nguoi_nop, :dia_chi, :ly_do_thu , :so_tien )
  end

  private

  def set_phieuchi
    @phieuchi = Phieuchi.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Phiếu chi bạn tìm không thấy"
    redirect_to phieuchis_path
    end

end
