class Admin::UsersController < Admin::BaseController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  def index
    @users = User.order(:email)
  end

  def show
  end

  def update
    # Update password if it's not blank
    if params[:user][:password].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end

    if @user.update(user_params)
      flash[:notice] = "User has been updated."
      redirect_to admin_users_path
    else
      flash[:alert] = "User has not been updated."
      render action: "edit"
    end
  end

  def edit
  end

  def new
    @user = User.new
  end

  def user_params
    params.require(:user).permit(:name,
    :email,
    :password,
    :password_confirmation,
    :position)
  end

  def destroy
    if @user == current_user
      flash[:alert] = "Can not delete current user!"
    else
      @user.destroy
      flash[:notice] = "User has been deleted."
    end
    redirect_to admin_users_path
  end

  private
  def set_user
    @user = User.find(params[:id])
  end

end
