﻿class PhieuthusController < ApplicationController
#before_action :authorize_admin!, except: [:index, :show]
before_action :set_phieuthu, only: [:show, :edit, :update, :destroy]
before_action :require_signin!
before_action :authorize_admin!

	def index
		@phieuthus = Phieuthu.all
	end
	
	def new
		@phieuthu = Phieuthu.new
	end
	
	def create
		@phieuthu = Phieuthu.new(phieuthu_params)
		if @phieuthu.save
			flash[:notice] = "Phiếu thu của bạn đã được tạo"
			redirect_to @phieuthu
		else
			flash[:alert] = "Phiếu thu không thể tạo."
			
			render "new"
		end
	end
	
	def show
		@phieuthu = Phieuthu.find(params[:id])
	end
	
	def edit
		@phieuthu = Phieuthu.find(params[:id])
	end
	
	def update
		@phieuthu = Phieuthu.find(params[:id])
		if @phieuthu.update(phieuthu_params)
			flash[:notice] = "Phiếu thu của ban đã được cập nhật."
			redirect_to @phieuthu
		else 
			flash[:alert] = "Cập nhật Phiếu thu thất bại."
			render "edit"
		end
	end
	
	def destroy
		@phieuthu = Phieuthu.find(params[:id])
		@phieuthu.destroy
		flash[:notice] = "Phiếu thu của bạn đã được xóa thành công"
		redirect_to phieuthus_path
	end
	
	private 
	
	def set_phieuthu
		@phieuthu = Phieuthu.find(params[:id])
	rescue ActiveRecord::RecordNotFound
		flash[:alert] = "Phiếu thu bạn tìm không thấy"
		redirect_to phieuthus_path
	end
	
	private
	def phieuthu_params
		params.require(:phieuthu).permit(:so_phieu, :ho_va_ten_nguoi_nop, :dia_chi, :ly_do_thu , :so_tien )
	end
	
	private 
	
	def set_phieuthu
		@phieuthu = Phieuthu.find(params[:id])
	rescue ActiveRecord::RecordNotFound
		flash[:alert] = "Phiếu thui bạn tìm không thấy"
		redirect_to phieuthus_path
	end
	
	

end
