module ApplicationHelper
  def admins_only(&block)
    block.call if current_user.try(current_user.isAdmin!)
  end
end
