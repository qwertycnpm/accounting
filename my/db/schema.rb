# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140520164407) do

  create_table "phieuchis", force: true do |t|
    t.string   "so_phieu"
    t.string   "ho_va_ten_nguoi_nhan"
    t.string   "dia_chi"
    t.string   "ly_do_chi"
    t.float    "so_tien"
    t.float    "da_thu"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "phieuthus", force: true do |t|
    t.string   "so_phieu"
    t.string   "ho_va_ten_nguoi_nop"
    t.string   "dia_chi"
    t.string   "ly_do_thu"
    t.float    "so_tien"
    t.string   "ma_phieuchi"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "per",             default: "normal"
  end

end
