class CreatePhieuchis < ActiveRecord::Migration
  def change
    create_table :phieuchis do |t|
      t.string :so_phieu
      t.string :ho_va_ten_nguoi_nhan
      t.string :dia_chi
      t.string :ly_do_chi
      t.float :so_tien
      t.float :da_thu
      
      
      t.timestamps
    end
  end
end
